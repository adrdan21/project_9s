from django.db import models
from django.utils.translation import gettext_lazy as _

class Pais(models.TextChoices):
        COLOMBIA = 'colombia', _('Colombia')
        ECUADOR = 'ecuador', _('Ecuador')
        VENEZUELA = 'venezuela', _('Venezuela')
        PERU = 'peru', _('Peru')

class Departamento(models.Model):

    nombre_dpto = models.CharField(null=False, blank=True, max_length=50)
    pais_id = models.CharField(max_length=50, default=Pais.COLOMBIA, 
    	choices=Pais.choices)

class Municipio(models.Model):

    nombre_mcpio = models.CharField(null=False, blank=True, max_length=50)
    departamento = models.ForeignKey(Departamento, default=None, on_delete = models.CASCADE)

