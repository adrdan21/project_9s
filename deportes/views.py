
from django.shortcuts import render,redirect

from rest_framework import generics
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import DeporteSerializer, EquipoSerializer
from deportes.models import Deporte, Equipo, Registro_Equipo

class DeporteViewSet(viewsets.ModelViewSet):
    queryset = Deporte.objects.all()
    serializer_class = DeporteSerializer

class EquipoViewSet(viewsets.ModelViewSet):
    queryset = Equipo.objects.all()
    serializer_class = EquipoSerializer
