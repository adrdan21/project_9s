from django.urls import path
from .views import DeporteViewSet, EquipoViewSet

urlpatterns = [
    path('reg_deportes/', DeporteViewSet.as_view({
        'get': 'list', 
        'post': 'create',
        'delete': 'destroy'
    })),
    path('listar_deportes/', DeporteViewSet.as_view({
        'get': 'list', 
    })),
    path('reg_equipos/', EquipoViewSet.as_view({
        'get': 'list', 
        'post': 'create',
        'delete': 'destroy'
    })),

]
