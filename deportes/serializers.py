
from rest_framework import serializers
from deportes.models import Deporte, Equipo, Registro_Equipo
from academicos.models import Programa, Semestre

class DeporteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deporte
        fields = ('id', 'nombre_deporte', 'cod_deporte', 'max_integrantes')

    def create(self, validate_data):
        
        instance = Deporte()

        instance.nombre_deporte = validate_data.get('nombre_deporte')
        instance.cod_deporte = validate_data.get('cod_deporte')
        instance.max_integrantes = validate_data.get('max_integrantes')

        instance.save()
        
        return instance

class EquipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equipo
        fields = ('id', 'nombre_equipo', 'siglas', 'programa', 'semestre', 'deporte')

    def create(self, validate_data):
        
        instance = Equipo()

        instance.nombre_equipo = validate_data.get('nombre_equipo')
        instance.siglas = validate_data.get('siglas')

        #Datos Academicos y Deportivos
        instance.programa = validate_data.get('programa')
        instance.semestre = validate_data.get('semestre')
        instance.deporte = validate_data.get('deporte')

        instance.save()
        
        return instance
