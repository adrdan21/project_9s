
from django.db import models
from usuarios.models import Persona
from academicos.models import Programa, Semestre

class Deporte(models.Model):

    nombre_deporte = models.CharField(null=False, blank=True, max_length=50)
    cod_deporte = models.CharField(null=False, blank=True, max_length=5)
    max_integrantes = models.IntegerField(null=False, blank=True)


class Equipo(models.Model):

    nombre_equipo = models.CharField(null=False, blank=True, max_length=50)
    siglas = models.CharField(null=False, blank=True, max_length=50)
    programa = models.ForeignKey(Programa, default=None, on_delete = models.CASCADE)
    semestre = models.ForeignKey(Semestre, default=None, on_delete = models.CASCADE)
    deporte = models.ForeignKey(Deporte, default=None, on_delete = models.CASCADE)


class Registro_Equipo(models.Model):

    persona = models.ForeignKey(Persona, default=None, on_delete = models.CASCADE)
    #semestre_id = models.CharField(null=True, blank=True, max_length=50)
    #programa_id = models.IntegerField(null=True, blank=True, max_length=50)
    #grupo_id_id = models.IntegerField(null=True, blank=True, max_length=50)
    #equipo = models.ForeignKey(Equipo, on_delete = models.CASCADE)