
from django.shortcuts import render,redirect

from rest_framework import generics
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import ProgramaSerializer, SemestreSerializer
from academicos.models import Sede, Programa, Semestre

class ProgramaViewSet(viewsets.ModelViewSet):
    queryset = Programa.objects.all()
    serializer_class = ProgramaSerializer

class SemestreViewSet(viewsets.ModelViewSet):
    queryset = Semestre.objects.all()
    serializer_class = SemestreSerializer