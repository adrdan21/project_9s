
from rest_framework import serializers
from academicos.models import Sede, Programa, Semestre

class SedeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sede
        fields = ('id', 'nombre_sede', 'municipio')

class ProgramaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Programa
        fields = ('id', 'nombre_programa', 'sede')

class SemestreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Semestre
        fields = ('id', 'nombre_semestre', 'abreviacion', 'grupo')
