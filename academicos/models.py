from django.db import models
from municipios.models import Municipio
from django.utils.translation import gettext_lazy as _

class Sede(models.Model):

    nombre_sede = models.CharField(null=False, blank=True, max_length=50)
    municipio = models.ForeignKey(Municipio, default=None, on_delete = models.CASCADE)

class Programa(models.Model):

    nombre_programa = models.CharField(null=False, blank=True, max_length=50)
    sede = models.ForeignKey(Sede, default=None, on_delete = models.CASCADE)


class Semestre(models.Model):
	class Grupo(models.TextChoices):
		GRUPO_A = 'a', _('A')
		GRUPO_B = 'b', _('B')
		GRUPO_C = 'c', _('C')
		GRUPO_D = 'd', _('D')

	nombre_semestre = models.CharField(null=False, blank=True, max_length=50)
	abreviacion = models.CharField(null=False, blank=True, max_length=50)
	grupo = models.CharField(max_length=50, default=Grupo.GRUPO_A, choices=Grupo.choices)


