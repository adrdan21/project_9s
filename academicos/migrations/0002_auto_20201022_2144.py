# Generated by Django 3.1.1 on 2020-10-23 02:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('academicos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='semestre',
            name='abreviacion',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='semestre',
            name='grupo',
            field=models.CharField(choices=[('a', 'A'), ('b', 'B'), ('c', 'C'), ('d', 'D')], default='a', max_length=50),
        ),
        migrations.AddField(
            model_name='semestre',
            name='nombre_semestre',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
