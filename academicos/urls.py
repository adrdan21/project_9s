from django.urls import path
from .views import ProgramaViewSet, SemestreViewSet

urlpatterns = [
    path('listar_programas/', ProgramaViewSet.as_view({
        'get': 'list', 
        'post': 'create',
        'delete': 'destroy'
    })),
    path('listar_semestres/', SemestreViewSet.as_view({
        'get': 'list', 
        'post': 'create',
        'delete': 'destroy'
    })),

]
