from django.urls import path
from .views import UserViewSet

urlpatterns = [
    path('reg_usuarios/', UserViewSet.as_view({
        'get': 'list', 
        'post': 'create',
        'delete': 'destroy'
    })),
        path('listar_usuarios/', UserViewSet.as_view({
        'get': 'list', 
    }), name = 'usuarios'),
]
