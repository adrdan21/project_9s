# Generated by Django 3.1.1 on 2020-10-23 02:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0005_auto_20201017_2243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='celular',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
