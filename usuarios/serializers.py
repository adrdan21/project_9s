from rest_framework import serializers
from django.contrib.auth.models import User
from usuarios.models import Persona


class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = ('id', 'nombre1', 'nombre2', 'apellido1', 'apellido2', 
            'identificacion', 'tipo_usuario', 'genero', 'tipo_doc',
            'celular', 'fecha_nac')


class UserSerializer(serializers.ModelSerializer):
    persona = PersonaSerializer(many = False)
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password','persona',)

    def create(self, validate_data):
        
        instance = User()

        instance.username = validate_data.get('username')
        instance.email = validate_data.get('email')
        instance.set_password(validate_data.get('password'))

        instance.save()

        #Datos de la persona
        persona_data = validate_data.pop('persona')
        persona = Persona()

        persona.nombre1 = persona_data['nombre1']
        persona.nombre2 = persona_data['nombre2']
        persona.apellido1 = persona_data['apellido1']
        persona.apellido2 = persona_data['apellido2']
        persona.identificacion = persona_data['identificacion']
        persona.tipo_usuario = persona_data['tipo_usuario']
        
        persona.genero = persona_data['genero']
        persona.tipo_doc = persona_data['tipo_doc']
        persona.celular = persona_data['celular']
        persona.fecha_nac = persona_data['fecha_nac']

        persona.user = instance
        persona.save()
        
        return instance

    def validate_identificacion(self, data):
        validar_identificacion = Persona.objects.filter(email = data)
        #print(validar_identificacion)
        if len(validar_identificacion) != 0:
            return serializers.ValidationError("El usuario ya cuenta con una cuenta existente")
        else:
            return data

