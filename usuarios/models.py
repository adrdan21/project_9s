from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

class Persona(models.Model):
    class TipoPersona(models.TextChoices):
        ADMIN = 'admin', _('Admin')
        GENERAL = 'general', _('General')

    nombre1 = models.CharField(null=True, blank=True, max_length=50)
    nombre2 = models.CharField(null=True, blank=True, max_length=50)
    apellido1 = models.CharField(null=True, blank=True, max_length=50)
    apellido2 = models.CharField(null=True, blank=True, max_length=50)
    tipo_doc = models.CharField(null=True, blank=True, max_length=50)
    identificacion = models.IntegerField(null=True, blank=True)
    tipo_usuario = models.CharField(max_length=50, default=TipoPersona.GENERAL, choices=TipoPersona.choices)
    celular = models.IntegerField(null=True, blank=True)
    fecha_nac = models.DateField(null=True, blank=True)
    genero = models.CharField(null=True, blank=True, max_length=50)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
