
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views
from usuarios.views import Login,Logout

urlpatterns = [
    path('admin/', admin.site.urls),
    path('registro/',include('usuarios.urls')),
    path('registro/',include('deportes.urls')),
    path('listar/',include('academicos.urls')),
    path('api_generador_token/',views.obtain_auth_token),
    path('login/',Login.as_view(), name = 'login'),
    path('logout/', Logout.as_view()),
]

